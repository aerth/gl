// The MIT License (MIT)
//
// Copyright (c) 2017 aerth aerth@riseup.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Command 'gl' Lite CLI GitLab Client
package main

import (
	"fmt"
	"os"

	//"github.com/kr/pretty"
	"gitlab.com/aerth/gl"
)

var token = os.Getenv("GITLAB_TOKEN")
func init(){
	fmt.Fprint(os.Stderr, "[gl] v0.0.0 - https://gitlab.com/aerth/gl\n")
	if token == "" {
		showhelp()
		fmt.Println("Please set GITLAB_TOKEN before using 'gl' command.")
		os.Exit(1)
	}
}
func main() {
	s := gl.NewSession(token)

	// require command
	if len(os.Args) < 2 {
		showhelp()
		fmt.Fprintln(os.Stderr, "fatal: need command")
		return
	}

	// switch command
	switch os.Args[1] {
	case "new":
		doNew(s, os.Args)
	case "list":
		doList(s, os.Args)
	case "whoami":
		usr, err := s.GetSession()
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(usr)
	case "vis", "visibility", "v", "visible":
		doVisibility(s, os.Args)
	default:
		showhelp()
	}

	// exit
}

func showhelp() {
	fmt.Fprintln(os.Stderr, "\tcommands: 'new' 'list' 'whoami' 'vis'")
	fmt.Fprintln(os.Stderr, "\t"+os.Args[0], "new", "repo_name")
	fmt.Fprintln(os.Stderr, "\t"+os.Args[0], "list")
	fmt.Fprintln(os.Stderr, "\t"+os.Args[0], "whoami")
	fmt.Fprintln(os.Stderr, "\t"+os.Args[0], "vis", "repo_name", "visibility_level")
	fmt.Fprintln(os.Stderr, "\n\tvisibility_level can be one of: private, public, internal\n")
}

func doNew(s *gl.Session, args []string) {
	var repo gl.Repository
	var err error
	switch len(args) {
	case 4:
		repo, err = s.RepositoryNew(args[2], args[3])
	case 3:
		repo, err = s.RepositoryNew("private", args[2])
	default:
		fmt.Println("error: need name of repository to create")
		return
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	fmt.Printf("[gl] %q created\n", repo.PathWithNamespace)
	fmt.Printf("[gl] %q link: %s\n", repo.Name, repo.WebURL)
	fmt.Printf("[gl] %q edit: %s/edit\n", repo.Name, repo.WebURL)
	fmt.Println(repo.Visibility)
	return
}

func doList(s *gl.Session, args []string) {

	repos, err := s.RepositoryList(args...)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	fmt.Printf("Found %v repositories:\n", len(repos))
	for _, repo := range repos {

		fmt.Printf("[%s] %s %s\n", repo.Visibility, repo.PathWithNamespace, repo.WebURL)
	}
	return

}
func doVisibility(s *gl.Session, args []string) {
	if len(args) != 4 {
		fmt.Println("example: gl vis test123 public")
		fmt.Println("example: gl vis test123 private")
		fmt.Println("example: gl vis test123 internal")
		return
	}
	switch args[3] {
	case "public":
	case "private":
	case "internal":
	default:
		fmt.Println("example: gl vis test123 public")
		fmt.Println("example: gl vis test123 private")
		fmt.Println("example: gl vis test123 internal")
		return
	}

	repo, err := s.RespositoryVisibility(args[2], args[3])
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%s: %s\n", repo.Name, repo.Visibility)
}
