# gl

This repository holds the library and associated CLI tool for use with Go + Gitlab API

## installation

To build the gl CLI tool from source using Go's 'go get', run this command:

``` go get -v -u gitlab.com/aerth/gl/tools/gl ```

*Otherwise...*

clone the repository, enter it, and run:

``` make && su -c 'make install' ```


### Binary releases

  * linux/amd64: https://dl.bintray.com/aerth/cmd/all/gl.tar.xz


### Contributing to `gl`

  * Keep it light
  

### Credits

  * Thank you GitLab (https://gitlab.com)
  * Type definitions "xanzy/go-gitlab" (https://github.com/xanzy/go-gitlab)
