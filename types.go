// The MIT License (MIT)
//
// Copyright (c) 2017 aerth aerth@riseup.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package gl

import "time"

/* types borrowed from "xanzy/go-gitlab" Apache v2.0 */
//
// Copyright 2015, Sander van Harmelen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// Namespace type
type Namespace struct {
	ID   int    `json:"id"`
	Name string `json:"name"`

	Path string `json:"path"`
	Kind string `json:"kind"`
}

// Contributor of project
type Contributor struct {
	Name      string `json:"name,omitempty"`
	Email     string `json:"email,omitempty"`
	Commits   int    `json:"commits,omitempty"`
	Additions int    `json:"additions,omitempty"`
	Deletions int    `json:"deletions,omitempty"`
}

// Project contains repo
type Project struct {
	ID                                        int               `json:"id"`
	Description                               string            `json:"description"`
	DefaultBranch                             string            `json:"default_branch"`
	Public                                    bool              `json:"public"`
	Visibility                                string            `json:"visibility"`
	SSHURLToRepo                              string            `json:"ssh_url_to_repo"`
	HTTPURLToRepo                             string            `json:"http_url_to_repo"`
	WebURL                                    string            `json:"web_url"`
	TagList                                   []string          `json:"tag_list"`
	Owner                                     *User             `json:"owner"`
	Name                                      string            `json:"name"`
	NameWithNamespace                         string            `json:"name_with_namespace"`
	Path                                      string            `json:"path"`
	PathWithNamespace                         string            `json:"path_with_namespace"`
	IssuesEnabled                             bool              `json:"issues_enabled"`
	OpenIssuesCount                           int               `json:"open_issues_count"`
	MergeRequestsEnabled                      bool              `json:"merge_requests_enabled"`
	ApprovalsBeforeMerge                      int               `json:"approvals_before_merge"`
	BuildsEnabled                             bool              `json:"builds_enabled"`
	WikiEnabled                               bool              `json:"wiki_enabled"`
	SnippetsEnabled                           bool              `json:"snippets_enabled"`
	ContainerRegistryEnabled                  bool              `json:"container_registry_enabled"`
	CreatedAt                                 *time.Time        `json:"created_at,omitempty"`
	LastActivityAt                            *time.Time        `json:"last_activity_at,omitempty"`
	CreatorID                                 int               `json:"creator_id"`
	Namespace                                 *ProjectNamespace `json:"namespace"`
	Permissions                               *Permissions      `json:"permissions"`
	Archived                                  bool              `json:"archived"`
	AvatarURL                                 string            `json:"avatar_url"`
	SharedRunnersEnabled                      bool              `json:"shared_runners_enabled"`
	ForksCount                                int               `json:"forks_count"`
	StarCount                                 int               `json:"star_count"`
	RunnersToken                              string            `json:"runners_token"`
	PublicBuilds                              bool              `json:"public_builds"`
	OnlyAllowMergeIfBuildSucceeds             bool              `json:"only_allow_merge_if_build_succeeds"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool              `json:"only_allow_merge_if_all_discussions_are_resolved"`
	LFSEnabled                                bool              `json:"lfs_enabled"`
	RequestAccessEnabled                      bool              `json:"request_access_enabled"`
	SharedWithGroups                          []struct {
		GroupID          int    `json:"group_id"`
		GroupName        string `json:"group_name"`
		GroupAccessLevel int    `json:"group_access_level"`
	} `json:"shared_with_groups"`
	Statistics *ProjectStatistics `json:"statistics"`
}

// Repository represents a repository.
type Repository struct {
	Name              string           `json:"name"`
	Description       string           `json:"description"`
	WebURL            string           `json:"web_url"`
	AvatarURL         string           `json:"avatar_url"`
	GitSSHURL         string           `json:"git_ssh_url"`
	GitHTTPURL        string           `json:"git_http_url"`
	Namespace         ProjectNamespace `json:"namespace"`
	Visibility        string           `json:"visibility"`
	PathWithNamespace string           `json:"path_with_namespace"`
	DefaultBranch     string           `json:"default_branch"`
	Homepage          string           `json:"homepage"`
	URL               string           `json:"url"`
	SSHURL            string           `json:"ssh_url"`
	HTTPURL           string           `json:"http_url"`
}

// ProjectNamespace represents a project namespace.
type ProjectNamespace struct {
	CreatedAt   *time.Time `json:"created_at"`
	Description string     `json:"description"`
	ID          int        `json:"id"`
	Name        string     `json:"name"`
	OwnerID     int        `json:"owner_id"`
	Path        string     `json:"path"`
	UpdatedAt   *time.Time `json:"updated_at"`
}

// StorageStatistics represents a statistics record for a group or project.
type StorageStatistics struct {
	StorageSize        int64 `json:"storage_size"`
	RepositorySize     int64 `json:"repository_size"`
	LfsObjectsSize     int64 `json:"lfs_objects_size"`
	BuildArtifactsSize int64 `json:"build_artifacts_size"`
}

// ProjectStatistics represents a statistics record for a project.
type ProjectStatistics struct {
	StorageStatistics
	CommitCount int `json:"commit_count"`
}

// Permissions represents premissions.
type Permissions struct {
	ProjectAccess *ProjectAccess `json:"project_access"`
	GroupAccess   *GroupAccess   `json:"group_access"`
}

// ProjectAccess represents project access.
type ProjectAccess struct {
	AccessLevel       int `json:"access_level"`
	NotificationLevel int `json:"notification_level"`
}

// GroupAccess represents group access.
type GroupAccess struct {
	AccessLevel       int `json:"access_level"`
	NotificationLevel int `json:"notification_level"`
}

// User represents user
type User struct {
	ID               int             `json:"id"`
	Username         string          `json:"username"`
	Email            string          `json:"email"`
	Name             string          `json:"name"`
	State            string          `json:"state"`
	CreatedAt        *time.Time      `json:"created_at"`
	Bio              string          `json:"bio"`
	Skype            string          `json:"skype"`
	Linkedin         string          `json:"linkedin"`
	Twitter          string          `json:"twitter"`
	WebsiteURL       string          `json:"website_url"`
	ExternUID        string          `json:"extern_uid"`
	Provider         string          `json:"provider"`
	ThemeID          int             `json:"theme_id"`
	ColorSchemeID    int             `json:"color_scheme_id"`
	IsAdmin          bool            `json:"is_admin"`
	AvatarURL        string          `json:"avatar_url"`
	CanCreateGroup   bool            `json:"can_create_group"`
	CanCreateProject bool            `json:"can_create_project"`
	ProjectsLimit    int             `json:"projects_limit"`
	CurrentSignInAt  *time.Time      `json:"current_sign_in_at"`
	LastSignInAt     *time.Time      `json:"last_sign_in_at"`
	TwoFactorEnabled bool            `json:"two_factor_enabled"`
	Identities       []*UserIdentity `json:"identities"`
}

// UserIdentity represents a user identity
type UserIdentity struct {
	Provider  string `json:"provider"`
	ExternUID string `json:"extern_uid"`
}
