// The MIT License (MIT)
//
// Copyright (c) 2017 aerth aerth@riseup.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Package gl is a minimal library for interacting with gitlab API
package gl

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

type Session struct {
	token   string
	client  *Client
	BaseURL string
	name string
	err     error
}

// NewSession where it starts
func NewSession(token string) *Session {
	if token == "" {
		return nil
	}
	session := new(Session)
	session.token = token
	session.BaseURL = "https://gitlab.com/api/v4/"
	session.client = new(Client)
	return session
}

// SetHTTPClient sets http client to use for all connections
func (s *Session) SetHTTPClient(client *Client) {
	s.client = client
}

// RepositoryNew creates a new repository
func (s *Session) RepositoryNew(visibility, name string) (repo Repository, err error) {
	req := s.request("POST", "projects", []byte(`{ "name": "`+name+`", "visibility": "`+visibility+`" }`))
	r, status, err := s.do(req)
	if err != nil {
		return Repository{}, err
	}
	if status != 201 {
		if strings.Contains(string(r), `"name":["has already been taken"]`) {
			return Repository{}, errors.New("Repository name already taken")
		}
		return Repository{}, errors.New(fmt.Sprint("repository not created: ", string(r)))
	}

	err = json.Unmarshal(r, &repo)
	if err != nil {
		return Repository{Name: err.Error()}, err
	}
	return repo, nil
}

// RepositoryList lists repositories, optionally filtering
func (s *Session) RepositoryList(filter ...string) (repos []Repository, err error) {
	req := s.request("GET", "projects?page=1&per_page=1000000&owned=1", nil)
	r, _, err := s.do(req)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(r, &repos)
	if err != nil {
		return nil, fmt.Errorf("JSON error: %s\nBody: %s\n", err, string(r))
	}

	return repos, nil
}

func (s *Session) GetSession() (stuff string, err error) {
	req := s.request("GET", "namespaces", nil)
	body, _, err := s.do(req)
	var ns []Namespace
	err = json.Unmarshal(body, &ns)
	if err != nil {
		return string(body), err
	}
	for _, namespace := range ns {
		if namespace.Kind == "user" {
			s.name = namespace.Name
			return namespace.Name, nil
		}
	}
	return "", errors.New("not found")
}

func (s *Session) DUMMY() (stuff string, err error) {
	req := s.request("POST", "session", nil)
	body, _, err := s.do(req)
	return string(body), nil
}

func (s *Session) GetRepository(id string) (repo Repository, err error) {
	req := s.request("GET", "/projects/"+id, nil)
	body, _, err := s.do(req)
	if err != nil {
		return repo, err
	}
	err = json.Unmarshal(body, &repo)
	return repo, err
}
func (s *Session) RespositoryVisibility(id string, visibility string) (repo Repository, err error) {
	if !strings.Contains(id, "/") {
		if s.name == "" {
		_, err := s.GetSession()
		if err != nil {
			repo.Name = "Error"
			return repo, err
			}
		}
		id = s.name + "%2F" + id
	}
	id = strings.Replace(id, "/", "%2F", -1)
	repo, err = s.GetRepository(id)
	repo.Visibility = visibility
	var b []byte
	b, err = json.Marshal(repo)
	req := s.request("PUT", "projects/"+id, b)
	body, _, err := s.do(req)
	if err != nil {
		return repo, err
	}
	err = json.Unmarshal(body, &repo)
	if err != nil {
		return repo, err
	}
	return s.GetRepository(id)
}
