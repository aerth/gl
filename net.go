// The MIT License (MIT)
//
// Copyright (c) 2017 aerth aerth@riseup.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package gl

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

// Client to connect
type Client struct {
	http.Client
}

func (s *Session) request(method, verb string, data []byte) *http.Request {
	if method == "" {
		method = "GET"
	}
	req, err := http.NewRequest(method, s.BaseURL+verb, bytes.NewBuffer(data))
	if err != nil {
		panic(err)
	}
	return req
}

func (s *Session) do(req *http.Request) (body []byte, status int, err error) {
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("PRIVATE-TOKEN", s.token)
	b, err := s.client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	body, err = ioutil.ReadAll(b.Body)
	b.Body.Close()
	status = b.StatusCode
	return body, status, err

}
